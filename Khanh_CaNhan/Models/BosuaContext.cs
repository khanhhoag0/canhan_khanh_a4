﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace Khanh_CaNhan.Models;

public partial class BosuaContext : DbContext
{
    public BosuaContext()
    {
    }

    public BosuaContext(DbContextOptions<BosuaContext> options)
        : base(options)
    {
    }

	public DbSet<Accounts> accounts { get; set; }
	public DbSet<Customer> customer { get; set; }
	public DbSet<Employees> employees { get; set; }
	public DbSet<Logs> logs { get; set; }
	public DbSet<Reports> reports { get; set; }
	public DbSet<Transactions> transactions { get; set; }

	protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see https://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Server=KHANH\\SQLEXPRESS;Database=BOSUA;Trusted_Connection=True;TrustServerCertificate=true;");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
