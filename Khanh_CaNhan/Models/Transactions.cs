﻿using System.ComponentModel.DataAnnotations;

namespace Khanh_CaNhan.Models
{
	public class Transactions
	{
		public int Id { get; set; }
		[Required, StringLength(100)]
		public string EmployeeID { get; set; }
		[Range(0.01, 10000.00)]
		public string CustomerID { get; set; }
		[Range(0.01, 10000.00)]
		public string Name { get; set; }
		public int AddTextHere { get; set; }


	}
}
